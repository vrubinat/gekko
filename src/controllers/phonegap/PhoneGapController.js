goog.provide('gecko.controllers.PhoneGapController');

goog.require('gecko.controllers.PhoneGapListController');
goog.require('gecko.controllers.PhoneGapDetailController');
goog.require('gecko.controllers.PhoneGapEditController');


/**
 * Function to create module PhoneGapController this module
 * work with phonegap plugin to save data in Device. 
 */
gecko.controllers.PhoneGapController = function() {

	var module = angular.module('PhoneGapController',[]);

	//asign Controlers to App scope

	module.controller('geckoListCtrl', ['$scope', gecko.controllers.PhoneGapListController]);

	module.controller('createCtrl', ['$scope', '$location', gecko.controllers.PhoneGapEditController]);

	module.controller('editCtrl', ['$scope', '$location', '$routeParams', gecko.controllers.PhoneGapEditController]);

	module.controller('geckoDetailCtrl', ['$scope', '$routeParams', gecko.controllers.PhoneGapDetailController]);

};