goog.provide('gecko.controllers.PhoneGapEditController');


/**
 * [PhoneGapEditController description]
 * @param {Object} $scope    [description]
 * @param {Object} $location [description]
 * @param {Object} $params [description]
 * @constructor
 */
gecko.controllers.PhoneGapEditController = function($scope, $location, $params) {



	if ($params && $params['bookId']) {
		cordova.exec(function(doc) {
				$scope['gecko'] = doc;
				$scope.$apply();
			}, function(error) {}, "DbPlugin",
			"get", [$params['bookId']]);

	}

	$scope['save'] = function(data) {
		cordova.exec(function(doc) {
				$location.path('/');
				this.location.hash = "#"; 
			}, function(error) {}, "DbPlugin",
			"put", [this['gecko']]);
	};


};