goog.provide('gecko.controllers.PouchListController');

goog.require('goog.array');
goog.require('gecko.conectors.pouchDB');
goog.require('goog.events');

/**
 * [PouchListController description]
 * @param {Object} $scope [description]
 * @constructor
 */
gecko.controllers.PouchListController = function($scope) {

	$scope['go'] = goog.bind(this.go, this, $scope);
	$scope['delete'] = goog.bind(this.del, this, $scope);

	var f = goog.bind(this.list, this, $scope);
	var pouchdb = gecko.conectors.pouchDB.getInstance();
	var db = pouchdb.getDB();
	db.allDocs({
		'include_docs': true
	}, f);

	goog.events.listen(pouchdb, gecko.conectors.eventType.CHANGE, function() {
		db.allDocs({
			'include_docs': true
		}, f);
	});

};

gecko.controllers.PouchListController.prototype.list = function($scope, err, doc) {
	var data = [];
	var d;
	goog.array.forEach(doc['rows'], function(doc) {
		if (doc['id'] !== "_design/views") {
			d = doc['doc'];
			d['id'] = doc['id'];
			data.push(d);
		}

	});
	$scope['books'] = data;
	$scope.$apply();

};

gecko.controllers.PouchListController.prototype.go = function($scope, data) {
	$scope['element'] = data;
};

gecko.controllers.PouchListController.prototype.del = function($scope, data) {


	var books = goog.array.filter($scope['books'], function(obj) {
		if (data === obj['id']) {
			return false;
		} else {
			return true;
		}
	});

	var db = gecko.conectors.pouchDB.getInstance().getDB();
	db.get(data, function(err, doc) {
		db.remove(doc, function(err, response) {
			$scope['books'] = books;
			$scope.$apply();
		});
	});


};