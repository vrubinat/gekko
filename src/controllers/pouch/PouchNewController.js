goog.provide('gecko.controllers.PouchNewController');

goog.require('gecko.conectors.pouchDB');

/**
 * [PouchNewController description]
 * @param {Object} $scope    [description]
 * @param {Object} $location [description]
 * @constructor
 */
gecko.controllers.PouchNewController = function($scope, $location) {


	$scope['save'] = function(data) {
		var db = gecko.conectors.pouchDB.getInstance().getDB();
		db.post(this['gecko'], function(err, response) {
			$location.path('/');
			this.location.hash = "#"; 
		});

	};


};