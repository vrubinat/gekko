goog.provide('gecko.controllers.UrlController');

goog.require('gecko.controllers.BookListController');
goog.require('gecko.controllers.DetailBookController');
goog.require('gecko.controllers.NewBooksController');
goog.require('gecko.controllers.EditBooksController');

/** 
 * Function to create module UrlController for work
 * with rest services
 */
gecko.controllers.UrlController = function() {

	var module = angular.module('UrlController',[]);

	//asign Controlers to App scope

	module.controller('geckoListCtrl', ['$scope', '$http', gecko.controllers.BookListController]);

	module.controller('createCtrl', ['$scope', '$http', '$location', gecko.controllers.NewBooksController]);

	module.controller('editCtrl', ['$scope', '$http', '$location', '$routeParams', gecko.controllers.EditBooksController]);

	module.controller('geckoDetailCtrl', ['$scope', '$http', '$routeParams', gecko.controllers.DetailBookController]);

};