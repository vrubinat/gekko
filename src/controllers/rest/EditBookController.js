goog.provide('gecko.controllers.EditBooksController');

/**
 * [NewBooksController description]
 * @param {Object} $scope    [description]
 * @param {Object} $http     [description]
 * @param {Object} $location [description]
 * @constructor
 */
gecko.controllers.EditBooksController = function($scope, $http, $location, $params) {


	$http.jsonp(geckoApp.url+'items/' + $params['bookId'] + '/?callback=JSON_CALLBACK').success(function(data) {

		$scope['gecko'] = data;
	
	});
	
	$scope['save'] = function(data) {
		
		$http.put(geckoApp.url+'update', this['gecko']).success(function(data) {
			$location.path('/');
		});

	};


};