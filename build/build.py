#!/usr/bin/python
# coding=utf-8

import os
import sys
import time 
import json
from optparse import OptionParser


parser = OptionParser()
parser.add_option("-v", "--version", dest="version", help="set version of build. example: 0.2.34")
parser.add_option("--doc", "--doc", dest="doc" , default="false" , help="Boolean value indicate is generate Docs, Default is false")
(options, args) = parser.parse_args()




WHITE = 0x000F
NICE_BEG = 0x003F
NICE_END = 0x00DF
# use design if the system is NT
if os.name == "nt":    
    from ctypes import *    
    windll.Kernel32.GetStdHandle.restype = c_ulong
    h = windll.Kernel32.GetStdHandle(c_ulong(0xfffffff5))
    colorFunc = windll.kernel32.SetConsoleTextAttribute
    colorFunc(h,NICE_BEG)
else :
    h = "nothing";
    def colorFunc(a,b):
        a = b
  
# funciones para la compilacion


def buildDebug():
  
    version = options.version
    
    pathExec = "../lib/closure-library/closure/bin/build/depswriter.py"
    gecko = ' --root_with_prefix=" ../src/ ../../../../src/ " '
    file ="../bin/gecko.js"
    
    cmd = "python " + pathExec + gecko  + "> " +file
    #print(cmd)
    os.system(cmd)

def build():
    
    
    version = options.version
                 
    file = 'gecko.min.'+version+'.js'      
   
#--compiler_flags="--externs=../lib/ol3/externs/bingmaps.js" \
        
    c = '../lib/closure-library/closure/bin/build/closurebuilder.py \
  --root=../lib/closure-library/ \
  --root=../src/ \
  --namespace=gecko \
  --output_mode=compiled \
  --compiler_jar=compiler.jar \
  --compiler_flags="--compilation_level=ADVANCED_OPTIMIZATIONS" \
  --compiler_flags="--output_wrapper=(function(){%output%})();" \
  --compiler_flags="--warning_level=VERBOSE" \
  --compiler_flags="--debug=false" \
  --compiler_flags="--define=goog.debug.LOGGING_ENABLED=true" \
  --compiler_flags="--externs=../build/externs/angular.js" \
  --compiler_flags="--externs=../build/externs/pouchdb.js" \
  --compiler_flags="--externs=../build/externs/cordova.js" \
  --output_file=../bin/'+file
  
    
    cmd = "python " + c
    print(cmd)
    os.system(cmd)

def buildCss():
    
    version = options.version
    cmd_css = "lessc ../less/gecko.less ../bin/gecko.min."+version+ ".css" + " -x"
    os.system(cmd_css)
    
 
#Se definen los colores
colorFunc(h,WHITE)
     
try:
    
    if options.version == None:
        print("El parametro -v es obligatorio")
        exit()
              
except:
    print ("Son necesarios los parametros del tipo y la versión")
    sys.exit()

print ("Empezando generación versión: "+options.version)
print os.getcwd()


buildDebug()
    
#buildCss()    
    
build() 

    
colorFunc(h,NICE_END)
print  (time.ctime())
colorFunc(h,WHITE)