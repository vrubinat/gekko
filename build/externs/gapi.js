/**
 * @const
 */
var gapi = {};


/**
 * @const
 */
gapi.auth = {};


/**
 *
 * @constructor
 */
var GapiClientDetails = function () {};

/**
 * @type {string}
 */
GapiClientDetails.prototype.client_id;


/**
 * @type {string}
 */
GapiClientDetails.prototype.scope;


/**
 * @type {boolean}
 */
GapiClientDetails.prototype.immediate;

/**
 * @type {string}
 */
GapiClientDetails.prototype.response_type;


/**
 *
 * @constructor
 */
var GapiAuthResult = function() {};


/**
 * @type {string}
 */
GapiAuthResult.prototype.access_token;

/**
 * @type {string}
 */
GapiAuthResult.prototype.error;

/**
 * @type {string}
 */
GapiAuthResult.prototype.expires_in;

/**
 * @type {string}
 */
GapiAuthResult.prototype.state;


/**
 *
 * @constructor
 */
var GapiToken = function() {};

/**
 * @type {string}
 */
GapiToken.prototype.state;

/**
 * @type {string}
 */
GapiToken.prototype.access_token;

/**
 * @type {string}
 */
GapiToken.prototype.token_type;

/**
 * @type {string}
 */
GapiToken.prototype.expires_in;

/**
 *
 * @constructor
 */
var GapiReturn = function (){};

/**
 * [execute description]
 * @param {Function} callback [description]
 */
GapiReturn.prototype.execute = function(callback){};
/**
 *
 * @param {Object} options
 * @param {function(GapiAuthResult)} callback
 */
gapi.auth.authorize = function(options, callback) {};


/**
 * @return {GapiToken}
 */
gapi.auth.getToken = function() {};

/**
 * @const
 */
gapi.client = {};

/**
 *
 * @param {string} key
 */
gapi.client.setApiKey = function(key) {};


/**
 *
 * @param {{
 *    path: string,
 *    method: (string|undefined),
 *    params: (Object|undefined),
 *    headers: (Object|undefined),
 *    body: (string|undefined),
 *    callback: (Function|undefined)}
 *  } args
 */
gapi.client.request = function(args) {};

gapi.client.load = function(type,version,callback){};

gapi.client.plus = {};

gapi.client.plus.people = {};

gapi.client.oauth2 = {};
gapi.client.oauth2.userinfo = {};

/**
 * [get description]
 * @return {GapiReturn} [description]
 */
gapi.client.oauth2.userinfo.get = function(){};
/**
 * 
 * @param {Object} param 
 * @return {GapiReturn} [description]
 */
gapi.client.plus.people.get = function(param){};


